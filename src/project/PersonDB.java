/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

import java.util.ArrayList;
import java.util.Scanner;


public class PersonDB {
static ArrayList person = new ArrayList<PersonDB>();
    private String name,surname,username,password,tel;
    private Double weight,height;

    public PersonDB(String name, String surname, String username, String password, String tel, Double weight, Double height) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.tel = tel;
        this.weight = weight;
        this.height = height;
    }

    public static ArrayList getPerson() {
        return person;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getTel() {
        return tel;
    }

    public Double getWeight() {
        return weight;
    }

    public Double getHeight() {
        return height;
    }
    
    

}
